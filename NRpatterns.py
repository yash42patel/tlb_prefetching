import random
import time
import numpy as np
								# 	TAGS HAVE A STRUCTURE {INT,FLAG(t/f)} i.e. used only in getPagePatterns()
class Trace:
    def __init__(self, fileAddr):
        self.fileAddr = fileAddr

    def readFile(self):
        #instructions = []
        dataAddr = []
        with open(self.fileAddr) as f:
            lines = f.readlines()
            for line in lines:
                #line = line1.split('\\')[0]            	
                if('miss' in line):# or len(str(line))>100 or len(str(line))<4):# or '\xb1' in line or line==''):
            		continue
                #if(len(line)<30): 
                fl=0
                try:
                    cs = (int(str(line)))
                    fl=1
            #print i
                except ValueError:
                    z='x'           	
                if(fl==1):
                    dataAddr.append(cs)
                '''data = line.split(' ')
                trace_type = int(data[0], 10)
                addr = int(data[1], 16)
                if trace_type in [0,1]:
                    dataAddr.append(addr)
                elif trace_type == 2:
                    instructions.append(addr)'''
                    
        self.dataAddr = np.array(dataAddr)
        #self.instructions = np.array(instructions)

    def getTags(self, num_bits):
    	#tmp = [bin(int(addr)) for addr in self.dataAddr]
    	self.tags=[]
    	for addr in self.dataAddr:
    		temp = bin(int(addr))
    		if(num_bits==0):
    			self.tags.append([int(temp,2),True])
	    	else:
    			if(len(str(temp))-2>num_bits):
    				self.tags.append([int(temp[:-num_bits],2),True])
    			else:
    				self.tags.append([int(0),True])
    	return self.tags

    def getTagStrides(self, tags, normalize,jump):
    	tag_strides=[]
    	#start=0
    	for start in range(jump+1):
    		p=[]
    		temp=[]
    		for k in range(start+jump+1, len(tags),jump+1):
    			temp.append([k,int(tags[k][0])-int(tags[k-(jump+1)][0])])
		    
    		tag_strides.append(temp)
    		#break
    	if not normalize:
    		return tag_strides
    	m = np.mean(tag_strides)
    	s = np.std(tag_strides)
    	return [(item-m)/s for item in tag_strides]
    	
def getStridePatterns(tag_strides):
	mainlis=[]
	for xx in range(len(tag_strides)):
			
		count=0
		lis=[]
		jump=0
		
		for i in range(0,len(tag_strides[xx])):
			if(i+1>=len(tag_strides[xx])):
				if(count>=1):
					for j in range(stindex,i+1):
						lis.append(tag_strides[xx][j])
						
					mainlis.append(lis)
					#print(lis)
				break
			
			
			
			#item = tag_strides[i]
			if((tag_strides[xx][i][1]==tag_strides[xx][i+1][1]) and tag_strides[xx][i][1]!=0):
				if(count==0):
					stindex = i
				count+=1
			
			elif(count>=1):	
				for j in range(stindex,i+1):
					lis.append(tag_strides[xx][j])
				mainlis.append(lis)
				count=0
				lis=[]
			else:	
				count=0
				lis=[]
			#print(lis)
			'''if(lis!=[]):
				print('found')
				break'''
   	return mainlis
   		#i+=(jump+1)


def getPagePatterns(tags,stridePatterns,jump):
	list1=[]
	for item in stridePatterns:
		temp=[]
		#print("\n\n Mystry is : "+str(tags[item[0][0]-jump-1]))
		if(tags[item[0][0]-jump-1][1]):
			temp.append(tags[item[0][0]-jump-1][0])
			tags[item[0][0]-jump-1][1] = True
		for x in item:
			if(tags[x[0]][1]):
				temp.append(tags[x[0]][0])
				tags[x[0]][1] = True
		if(temp!=[]):
			list1.append(temp)
	return list1	

def main():
	trace = Trace('specrand999.txt')
	trace.readFile()
	dataAddr = trace.dataAddr
	#print(dataAddr)
	#print('\n\n')
	num_bits = 12
	tags = trace.getTags(num_bits)
	#print(tags)
	#print('\n\n')	
	#i = int(input("Enter an integer "));
	#for b in range(i,i+1):
	for b in range(0,5):
		print('\n\nPatterns with stride = '+str(b))
		tag_strides = trace.getTagStrides(tags, False,b)		#return [i,diff between i-(i-jump)] 
		#print(tags)
		#print('\n\n Tag strides')
		
		
		
		#print(tag_strides)
		#int cfreq=3
		#print('\n\n')
		#print('strides')
		stridePatterns = getStridePatterns(tag_strides)
		#print(stridePatterns)
		
		pagePatterns = getPagePatterns(tags,stridePatterns,b)
		
		# ----------CALCULATING REPORTING-----------
		numbers=[]
		for item in pagePatterns:
			numbers.append(len(item))
			#print(item)
		
		#print numbers	
		numberset=set(numbers)
		frequency=[]
		
		#print numberset
		for item in numberset:
			if(numbers.count(item)>1 and item>1):
				frequency.append([item,numbers.count(item)])
		
		# ----------REPORTING-----------
		for item in frequency:
			print(item)
		
		
	
if __name__ == "__main__":
	main()

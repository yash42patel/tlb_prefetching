import random
import time
import numpy as np
import copy

chaar=[]
paanch=[]
chhe=[]
newchaar=[]
newpaanch=[]
newchhe=[]
count4=0
count5=0
count6=0
fullcount4=0
fullcount5=0
fullcount6=0
class Trace:
    def __init__(self, fileAddr):
        self.fileAddr = fileAddr

    def readFile(self):
        #instructions = []
        dataAddr = []
        index=0
        with open(self.fileAddr) as f:
            lines = f.readlines()
            for line in lines:
            	if('miss' in line):
            		if(index>=4):
            			tempfour=[dataAddr[index-4],dataAddr[index-3],dataAddr[index-2],dataAddr[index-1]]
            			chaar.append(tempfour)
            		if(index>=5):
            			tempfive=[dataAddr[index-5],dataAddr[index-4],dataAddr[index-3],dataAddr[index-2],dataAddr[index-1]]
            			paanch.append(tempfive)
            		if(index>=6):
            			tempsix=[dataAddr[index-6],dataAddr[index-5],dataAddr[index-4],dataAddr[index-3],dataAddr[index-2],dataAddr[index-1]]
            			chhe.append(tempsix)
            		continue
            	
            	#print line
            	dataAddr.append(int(str(line)))
            	index+=1
        
                    
        self.dataAddr = np.array(dataAddr)
        #print chaar
        #self.instructions = np.array(instructions)

    def getTags(self, num_bits):
    	#tmp = [bin(int(addr)) for addr in self.dataAddr]
    	self.tags=[]
    	for addr in self.dataAddr:
    		temp = bin(int(addr))
    		if(num_bits==0):
    			self.tags.append(int(temp,2))
	    	else:
    			if(len(str(temp))-2>num_bits):
    				self.tags.append(int(temp[:-num_bits],2))
    			else:
    				self.tags.append(int(0))
    	for addr in chaar:
    		v=''
    		for item in addr:
    			temp = bin(int(item))
    			if(num_bits==0):
    				v+=str((int(temp,2)))
    			else:
    				if(len(str(temp))-2>num_bits):
    					v+=str((int(temp[:-num_bits],2)))
    				else:
    					v+=str(0)
    			v+=' '
    		newchaar.append(v)
    	#print newchaar
    	for addr in paanch:
    		v=''
    		for item in addr:
    			temp = bin(int(item))
    			if(num_bits==0):
    				v+=str((int(temp,2)))
    			else:
    				if(len(str(temp))-2>num_bits):
    					v+=str((int(temp[:-num_bits],2)))
    				else:
    					v+=str(0)
    			v+=' '
    		newpaanch.append(v)
    	for addr in chhe:
    		v=''
    		for item in addr:
    			temp = bin(int(item))
    			if(num_bits==0):
    				v+=str((int(temp,2)))
    			else:
    				if(len(str(temp))-2>num_bits):
    					v+=str((int(temp[:-num_bits],2)))
    				else:
    					v+=str(0)
    			v+=' '
    		newchhe.append(v)
    	return self.tags

    def getTagStrides(self, tags, normalize,jump):
    	tag_strides=[]
    	#start=0
    	for start in range(jump+1):
    		p=[]
    		temp=[]
    		for k in range(start+jump+1, len(tags),jump+1):
    			temp.append([k,int(tags[k][0])-int(tags[k-(jump+1)][0])])
		    
    		tag_strides.append(temp)
    		#break
    	if not normalize:
    		return tag_strides
    	m = np.mean(tag_strides)
    	s = np.std(tag_strides)
    	return [(item-m)/s for item in tag_strides]

def getPatterns4(tags):
	mainlis=[]
	count=0
	lis=[]
	string = ''
	#print string
	combo=4
	
	for i in range(len(tags)-combo+1):
		j=i
		string=""
		while(j<len(tags)):
			string= string + str(tags[j])+" "
			j=j+1
		#print "STRING"
		#print string
		subString=""
		if(tags[i]==tags[i+1] and tags[i+1]==tags[i+2] and tags[i+2]==tags[i+3]):
			a= ''
		else:
			subString = subString + str(tags[i])+" "+str(tags[i+1])+" "+str(tags[i+2])+" "+str(tags[i+3])+" "
			#print subString
			a=string.count(subString)
			#print a
			if(a>3): 
				lis.append([subString,a])
	#print lis
	
	count4=0
	newchaarset=set(newchaar)
	flag=0
	fullcount4=0
	for item in newchaar:
		flag=0
		for items in lis:
			if(items[0]==item):
				flag=1
			#print item," ",items[0]	
		if(flag==1):
			count4+=1
	print "count ",count4
	print "full count ",len(newchaar)
				
			
	return lis

def getPatterns5(tags):
	mainlis=[]
	count=0
	lis=[]
	string = ''
	#print string
	combo=5
	
	for i in range(len(tags)-combo+1):
		j=i
		string=""
		while(j<len(tags)):
			string= string + str(tags[j])+" "
			j=j+1
		subString=""
		if(tags[i]==tags[i+1] and tags[i+1]==tags[i+2] and tags[i+2]==tags[i+3] and tags[i+3]==tags[i+4]):
			a= ''
		else:
			subString = subString + str(tags[i])+" "+str(tags[i+1])+" "+str(tags[i+2])+" "+str(tags[i+3])+" "+str(tags[i+4])+" "
			#print subString
			a=string.count(subString)
			#print a
			if(a>3): 
				lis.append([subString,a])
	#print lis	
	count5=0
	newpaachset=set(newpaanch)
	flag=0
	fullcount5=0
	for item in newpaanch:
		flag=0
		for items in lis:
			if(items[0]==item):
				flag=1
		if(flag==1):
			count5+=1
	print "count ",count5	
	print "full count ",len(newpaanch)
	
	return lis

def getPatterns6(tags):
	mainlis=[]
	count=0
	lis=[]
	string = ''
	#print string
	combo=6
	
	for i in range(len(tags)-combo+1):
		j=i
		string=""
		while(j<len(tags)):
			string= string + str(tags[j])+" "
			j=j+1
		subString=""
		if(tags[i]==tags[i+1] and tags[i+1]==tags[i+2] and tags[i+2]==tags[i+3] and tags[i+3]==tags[i+4] and tags[i+4]==tags[i+5]):
			a= ''
		else:
			subString = subString + str(tags[i])+" "+str(tags[i+1])+" "+str(tags[i+2])+" "+str(tags[i+3])+" "+str(tags[i+4])+" "+str(tags[i+5])+" "
			#print subString
			a=string.count(subString)
			#print a
			if(a>3): 
				lis.append([subString,a])
		
	#print lis
	count6=0
	newchheset=set(newchhe)

	fullcount6=0
	flag=0
	for item in newchhe:
		flag=0
		for items in lis:
			if(items[0]==item):
				flag=1
		if(flag==1):
			count6+=1
	print "count ",count6
	print "fullcount ",len(newchhe)

	return lis



def main():
	trace = Trace('addresses_10hazar.txt')
	trace.readFile()
	dataAddr = trace.dataAddr
	num_bits = 12
	tags = trace.getTags(num_bits)
	#print("TAgs ")
	#print tags
	
	
	patterns=[]
	print ("4CGES")
	lis=getPatterns4(tags)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
		#print(item)
	print ("5CGES")
	lis=getPatterns5(tags)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
		#print(item)

	print ("6CGES")
	lis=getPatterns6(tags)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
		#print(item)
	
	tempmask=7
	tags3bit=copy.deepcopy(tags)
	for i in range(len(tags3bit)):
		tags3bit[i]= tags3bit[i] & tempmask
	#print tags3bit
	#print "tags3bit"
	#print tags3bit

	print ("34CGES")
	lis=getPatterns4(tags3bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
		#print(item)
	
	print ("35CGES")
	lis=getPatterns5(tags3bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
	
	print ("36CGES")
	lis=getPatterns6(tags3bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
	
	
	tempmask=15
	tags4bit=copy.deepcopy(tags)
	for i in range(len(tags4bit)):
		tags4bit[i]= tags4bit[i] & tempmask
	#print tags4bit
	#print "tags4bit"
	#print tags4bit


	print ("44CGES")
	lis=getPatterns4(tags4bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
		#print(item)
	
	print ("45CGES")
	lis=getPatterns5(tags4bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
	
	print ("46CGES")
	lis=getPatterns6(tags4bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]


	tempmask=31
	tags5bit=copy.deepcopy(tags)

	for i in range(len(tags5bit)):
		tags5bit[i]= tags5bit[i] & tempmask
	#print tags5bit
	#print "tags5bit"
	#print tags5bit

	print ("54CGES")
	lis=getPatterns4(tags5bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
		#print(item)
	
	print ("55CGES")
	lis=getPatterns5(tags5bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]
	
	print ("56CGES")
	lis=getPatterns6(tags5bit)
	#print lis
	temp=[]
	finalAnalysis=[]
	countList=[]
	for i in range(len(lis)):
		temp.append(lis[i][1])
	temp1=set(temp)
	countList=list(temp1)
	for i in range(len(countList)):
		count=0
		for j in range(len(lis)):
			if(lis[j][1]==countList[i]):
				count=count+1
		finalAnalysis.append([countList[i],count])
	for i in range(len(finalAnalysis)):	
		print finalAnalysis[i]


if __name__ == "__main__":
	main()

# TLB_PREFETCHING

Using stride and recency for TLB Prefetching

1) In Gem5 Simulator we have changed the code of a file tlb.cc in X86 system architecture folder, the same file is attached in the repository by the name 'tlb.cc' and this file was used to get virtual address and missed page.
2) For the analysis of the patterns in VPN based on recency, a python script was written, its also attached namely "Recency_Pattern.py". Running this code in python2.7 using the commandline in UBUNTU "python Recency_Pattern.py", name of the file on which analysis is to be done is to be updated inside .py file.
3) For the analysis of the patterns in stride, a python script was written, its also attached namely "NRpatterns.py". Running this code in python2.7 using the commandline in UBUNTU "python NRpatterns.py", name of the file on which analysis is to be done is to be updated inside .py file.
4) The input to each of these files is also uploaded on git namely, demo.txt, which contains the Virtual addresses obtained in chuncks of 20000 using the head command on the original file that was obtaineed from the tlb.cc in gem5 and the code for conversion of virtual addresses to virtual page numbers is already written in the script, as our goal is to perform analysis of the virtual page numbers.Exapmle the file obtained after running benchmark on gem5 ,namely "1.txt" (i.e. a bulky file of about 10GB-20GB) to divide it into chunks of 10000 and 20000 we have written a script i.e. script.sh which is also uploaded.
5) Graphs obtained are also uploaded on the gitlab.
6) A code was written to obtain the data from which graphs can be built, over the data that was given by the two python scripts, the code is in file namely "mix.py".
7) The output returned by mix.py is also uploaded on gitlab in the folder namely OUTPUTMAIN.

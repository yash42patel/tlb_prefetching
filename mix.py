import random
import time
import numpy as np
import copy

def main():
	mix=[]
	node=[]
	tempsum=[]
	
	for i in range(0,1001):
		tempsum.append([i,0])
	for i in range(12):
		mix.append(copy.deepcopy(tempsum))
	
	for fint in range(1,67):
		strfname=str(fint)
		mixindex=-1
		with open(strfname) as f:
			lines = f.readlines()
			#flag=True
			for line in lines:
				if('CGES' in line):
					mixindex+=1
					continue
				elif('count' in  line):
					continue
				else:
					core=line.strip().split()
					index=int(core[0][1:-1])
					value=int(core[1][0:-1])
					if(index>1000):
						continue
					mix[mixindex][index][1]+=value
			f.close()
	fr=open("outputs", "w")

	for h in range(12):
		string = '\n\n--------------------------------- VALUES of '+str(h+1)+'------------------------------------'
		fr.write(string)
		string = '\n'
		fr.write(string)
		#print '\n\n'
		for item in range(4,251):
			xx=mix[h][item]
			#if(int(xx[1])<>0):
			string = '\n'+str(xx[1])
			fr.write(string)
if __name__ == "__main__":
	main()
